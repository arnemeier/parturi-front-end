async function login(credentials) {
    let response = await fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(credentials)
        }
    );
    // console.log(JSON.stringify(credentials));
    return await handleJsonResponse(response);
}

async function postNewUser(newUser) {
    // console.log(JSON.stringify(newUser));
    let response = await fetch(
        `${API_URL}/users/register`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(newUser)
        }
    );
    // console.log(response);
    return await handleJsonResponse(response);
}

async function fetchUserData(email) {
    let response = await fetch(
        `${API_URL}/users/${email}`,
        {
            method: 'GET',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    return await handleJsonResponse(response);
}

async function postUserUpdates(updatedUser) {
    let response = await fetch(
        `${API_URL}/users/update/`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // 'Accept': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(updatedUser)
        }
    );
    let updatedToken = await response.text();

    if (updatedToken != '') {
        let updatedSession = {
            email: updatedUser.email,
            token: updatedToken
        };
    
        storeAuthentication(updatedSession);
    
    } else {
        renderAlert('Probleem andmete uuendamisega');
    }
}

// async function fetchUserId(email) {
//     let response = await fetch(
//         `${API_URL}/users/update/`,
//         {
//             method: 'GET',
//             headers: { 'Authorization': `Bearer ${getToken()}` }
//         }


//     );
// }