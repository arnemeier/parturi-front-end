/* 
    --------------------------------------------
    INIT
    --------------------------------------------
*/
var calendar = null;
let userBookings = [];
window.addEventListener('DOMContentLoaded', () => {
    const mainContainer = document.querySelector("#mainContainer");
    loadHeader();
    if (getToken()) {
        updateHeader();
        loadCurrentUserOffersContainer();
        loadCurrentUserOffers();
    }
    loadOffers();


});

/* 
    --------------------------------------------
    LOGIN/REGISTER FUNCTIONS
    -------------------------------------------- 
*/

async function doLogin() {
    // loadLoginForm();
    try {
        const credentials = {
            email: document.querySelector('#inputEmail').value,
            password: document.querySelector('#inputPassword').value,
        };
        const session = await login(credentials);
        storeAuthentication(session);
        clearLoginForm();
        updateHeader();
        loadCurrentUserOffersContainer();
        loadCurrentUserOffers();
    } catch (e) {
        handleError(e, renderLoginAlert('Vale kasutaja või salasõna'));
    }
}

function doLogout() {
    reload();
    removeCurrentUserOffers();
    clearSelectedOffer();
    loadOffers();
    clearAuthentication();
    updateHeader();
}

async function createNewuser() {
    if (document.querySelector('.alert')) {
        closeAllAlerts();
    }
    try {
        let newUser = {
            firstName: document.querySelector('#inputFirstName').value,
            lastName: document.querySelector('#inputLastName').value,
            email: document.querySelector('#inputEmail').value,
            password: document.querySelector('#inputPassword').value,
            passwordRetyped: document.querySelector('#inputRepeatPassword').value,
            phone: document.querySelector('#inputPhone').value,
        };

        let validationResponse = validateRegistration(newUser);

        if (validationResponse.length == 0) {
            let response = await postNewUser(newUser);
            if (response.errors.length == 0) {
                renderSuccess("Registreerimine õnnestus!");
                setTimeout(function () {
                    alertClose();
                    loadLoginForm();
                }, 2500);
            } else {
                renderAlert(response.errors[0]);
            }
        } else {
            validationResponse.forEach(error => {
                renderAlert(error);
            });
        }

    } catch (error) {
        handleError(error, function () {
            loadHeader();
            loadOffers();
        });
    }
}

async function updateUser() {
    if (document.querySelector('.alert')) {
        closeAllAlerts();
    }
    try {
        let updatedUser = {
            firstName: document.querySelector('#inputFirstName').value,
            lastName: document.querySelector('#inputLastName').value,
            email: document.querySelector('#inputEmail').value,
            password: document.querySelector('#inputPassword').value,
            passwordRetyped: document.querySelector('#inputRepeatPassword').value,
            phone: document.querySelector('#inputPhone').value,
        }

        let validationResponse = validateRegistration(updatedUser);

        if (validationResponse.length == 0) {
            let updateResponse = await postUserUpdates(updatedUser);
            renderSuccess("Andmete muutmine õnnestus!");
            setTimeout(function () {
                alertClose();
                clearRegisterForm();
            }, 2500);

        } else {
            validationResponse.forEach(error => {
                renderAlert(error);
            });
        }
    } catch (error) {
        handleError(error, renderAlert('probleem registreermisega'));
    }
}

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function loadHeader() {
    let navHeader = document.querySelector('#navBar').content;
    document.body.append(navHeader.cloneNode(true));
}

function updateHeader() {
    let navHeaderUsername = document.querySelector('header h6');
    let navHeaderRegisterLink = document.querySelector('#pleaseRegister');
    let navHeaderLoginToggle = document.querySelector('#loginToggle');
    if (!getToken()) {
        navHeaderUsername.innerHTML = '';
        navHeaderLoginToggle.innerHTML = 'Logi sisse';
        navHeaderLoginToggle.href = 'javascript:loadLoginForm()';
        navHeaderRegisterLink.innerHTML = 'Registreeri';
    } else {
        navHeaderUsername.innerHTML = /*html*/`
        <a href="javascript:fillUserData()">${getUsername()}</a>
        `;
        navHeaderRegisterLink.innerHTML = '';
        navHeaderLoginToggle.innerHTML = 'Logi välja';
        navHeaderLoginToggle.href = 'javascript:doLogout()';
    }
}

async function loadOffers() {
    let allOffers = document.querySelector('#availableOffers').content;
    mainContainer.appendChild(allOffers.cloneNode(true));
    let offerContainer = document.querySelector('#offers');
    let offers = await fetchOffers();
    // console.log(offers);
    let offerCardHtml = /*html*/`
    
    `;

    offers.forEach(offer => {
        // console.log(offer);
        offerCardHtml += /*html*/`
        <div class="col-md-4">
        <div class="card mb-4 shadow-sm">
            <img src="${offer.profilePictureUrl}" class="card-img-top" alt="...">

          <div class="card-body">
            <p class="card-text lead">${offer.services}</p>
            <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group">
                <!-- <button type="button" class="btn btn-sm btn-outline-secondary">View</button> -->
                <button type="button" class="btn btn-sm btn-outline-primary" onclick="makeBooking(${offer.id})" ;>Broneeri</button>
              </div>
              <div class="info-group">

                <!-- <small class="text-muted">Umbes 15 euri</small> -->
                <!-- <small class="text-muted">2h aega</small> -->
              </div>
            </div>
          </div>
        </div>
      </div>

        `;
    });
    offerContainer.innerHTML = offerCardHtml;

}

function removeOffers() {
    document.querySelector('#offers').remove();
}

function loadCurrentUserOffersContainer() {
    let currentUseOffersContainer = document.querySelector('#userOffersContainerTemplate').content;
    mainContainer.prepend(currentUseOffersContainer.cloneNode(true));

}

async function loadCurrentUserOffers() {
    let currentUseOffers = document.querySelector('#userOffersTemplate').content;
    let currentUserData = await fetchUserData(getUsername());

    let currentUserId = currentUserData.id;
    userBookings = await fetchSelectedUserBookings(currentUserId);
    console.log(userBookings.length);

    if (userBookings.length > 0) {

        // console.log(JSON.stringify(userBookings).sort((a,b)=> a-b));
        // console.log(JSON.parse(userBookings).sort((a,b) => a-b));
        userBookings.sort((a, b) => moment(a.start_at, "YYYY-MM-DD hh:mm:ss").valueOf() - moment(b.start_at, "YYYY-MM-DD hh:mm:ss").valueOf());
        userBookings.forEach(element => {
            let currentDivFragment = document.importNode(currentUseOffers, true);
            // currentDivFragment.querySelector("*").innerHTML = "asdfasdf";
            document.querySelector('#currentUserOffers .row').prepend(currentDivFragment);
            let currentDiv = document.querySelector('#currentUserOffers .row').querySelector("*");

            currentDiv.querySelector('.card-text').innerHTML = /*html*/` 
        <h5>${element.service_name}</h5>
        <p>${moment(element.start_at).format('DD.MM.YYYY HH:mm')} kuni ${moment(element.end_at).format('HH:mm')}</p>
        <p></p>        
        `;
            currentDiv.querySelector('.btn').setAttribute('onclick', 'viewBooking(' + element.id + ')');
        });
    } else {
        document.querySelector('#currentUserOffers').remove();
    }
}

async function viewBooking(bookingId) {
    removeCurrentUserOffers();
    removeOffers();

    let currentBooking = document.querySelector('#viewBookingTemplate').content;
    mainContainer.prepend(currentBooking.cloneNode(true));

    currentBookingEvent = userBookings.filter(function (element) {
        return element.id == bookingId;
    })[0];

    let currentCard = document.querySelector('#viewBooking > div > .card');
    // currentCard.querySelector('.card-body').innerHTML += 'test';

    currentCard.querySelector('.card-body').innerHTML = /*html*/` 
    <h5>${currentBookingEvent.service_name}</h5>
    <p>Kuupäev: ${moment(currentBookingEvent.start_at).format('DD.MM.YYYY')}</p>
    <p>Algusaeg: ${moment(currentBookingEvent.start_at).format('HH:mm')}</p>
    <p>Lõpuaeg:  ${moment(currentBookingEvent.end_at).format('HH:mm')}</p>
    <p>Eeldatav maksumus: ${currentBookingEvent.price}€</p>
    <p>Salongi asukoht:  ${currentBookingEvent.street} tn ${currentBookingEvent.building}, ${currentBookingEvent.city}</p>
    <br>
    <p>Palun tule 5min enne õiget aega!</p>
    
    <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group">
                <!-- <button type="button" class="btn btn-sm btn-outline-secondary">View</button> -->
                <button type="button" class="btn btn-sm btn-outline-danger" onclick="cancelBooking(${currentBookingEvent.id})" ;="">Tühista</button>
              </div>
              <div class="info-group">

                <small class="text-muted">Tühistamisel arvesta tasuga 2€</small>
                <!-- <small class="text-muted">2h aega</small> -->
              </div>
            </div>

    `;
    initMap();


}

function removeCurrentUserOffers() {
    if (document.querySelector('#currentUserOffers')) {
        document.querySelector('#currentUserOffers').remove();
    }
}

function loadLoginForm() {
    if (!document.querySelector('#form-signin')) {

        if (document.querySelector('#form-register')) {
            clearRegisterForm();
        }
        let loginFormTemplate = document.querySelector('#signin').content;
        mainContainer.prepend(loginFormTemplate.cloneNode(true));
    }
}

function clearLoginForm() {
    document.querySelector('#form-signin').remove();
}

function loadRegisterForm() {
    if (!document.querySelector('#form-register')) {
        if (document.querySelector('#form-signin')) {
            clearLoginForm();
        }
        let registerFormTemplate = document.querySelector('#register').content;
        mainContainer.prepend(registerFormTemplate.cloneNode(true));
    }
}

function clearRegisterForm() {
    document.querySelector('#form-register').remove();
}

function renderSelectedOffer() {
    let selectedOfferTemplate = document.querySelector('#currentOfferTemplate').content;
    mainContainer.prepend(selectedOfferTemplate.cloneNode(true));
    initMap();
}

function clearSelectedOffer() {
    if (document.querySelector('#currentOffer')) {
        document.querySelector('#currentOffer').remove()
    }
}

function closeCloseForm() {
    document.querySelector('.form-signin').remove();
}

async function fillUserData() {
    loadRegisterForm();
    let response = await fetchUserData(getUsername());
    document.querySelector("#inputFirstName").value = response.firstName;
    document.querySelector("#inputLastName").value = response.lastName;
    document.querySelector("#inputEmail").value = response.email;
    document.querySelector("#inputPhone").value = response.phone;
    document.querySelector('#newUserSubmit').setAttribute('onclick', 'updateUser()');
}

function reload() {
    mainContainer.innerHTML = '';
    loadOffers();
    loadCurrentUserOffersContainer();
    loadCurrentUserOffers();
}

/* 
    --------------------------------------------
    BOOKING-RELATED FUNCTIONS
    --------------------------------------------
*/

async function makeBooking(hairdresser) {
    // console.log('hairdresser: ' + hairdresser);
    // console.log('schedules: ');
    if (!getToken()) {
        loadLoginForm();
    }

    else {
        let calendarTemplate = document.querySelector('#calendarTemplate').content;
        let draggableTemplate = document.querySelector('#draggableEventsTemplate').content;
        renderSelectedOffer();
        removeCurrentUserOffers();
        removeOffers();
        mainContainer.prepend(calendarTemplate.cloneNode(true));
        document.querySelector('#calendarContainer').prepend(draggableTemplate.cloneNode(true));

        let bh = await loadBusinessHours(hairdresser);
        let draggedEvents = [];


        let draggableElement = document.querySelector('#draggableEvents');
        let asi = await insertServicesIntoDraggableMenu(hairdresser);

        new FullCalendarInteraction.Draggable(
            draggableElement, {
            itemSelector: '.fc-event',
            eventData: function (eventEl) {
                return {
                    title: eventEl.innerHTML,
                    duration: eventEl.dataset.duration,
                    overlap: false,
                    durationEditable: false,
                    extendedProps: {
                        "serviceid": eventEl.dataset.serviceid
                    }
                };
            }
        }
        );

        let calendarElement = document.querySelector('#calendar');
        calendar = new FullCalendar.Calendar(calendarElement, {
            plugins: ['interaction', 'timeGrid'],
            defaultView: 'timeGrid',
            height: "parent",
            contentHeight: 550,
            visibleRange: {
                start: moment().add(1, 'days').format('YYYY-MM-DD'),
                end: moment().add(8, 'days').format('YYYY-MM-DD')
            },
            views: {
                timeGrid: { // name of view
                    allDaySlot: false,
                    minTime: "07:00:00",
                    maxTime: "23:00:00",
                }
            },
            header: {
                right: ''
            },
            businessHours: bh,
            eventConstraint: "businessHours",
            locale: 'et',
            firstDay: 1,
            editable: true,
            droppable: true,
            slotDuration: '00:30:00',
            snapDuration: '00:05:00',
            slotLabelInterval: '01:00',
            eventReceive: function (info) {
                draggedEvents.push(info);
                let buttons = document.querySelectorAll('#cancelorpay > button');
                buttons.forEach(button => {
                    button.disabled = false;
                });
            }
        });
        let bgEvents = await addBackgroundEvent(hairdresser);
        bgEvents.forEach(bgEvent => {
            calendar.addEvent(bgEvent);
        });

        calendar.render();

        let cancelButton = document.querySelector('#cancelAllBookings');
        cancelButton.addEventListener('click', event => {

            calendar.getEvents().forEach(calEvent => {
                if (calEvent.rendering != 'background') {
                    calEvent.remove();
                }
            });

            document.querySelectorAll('#cancelorpay > button').forEach(button => {
                button.disabled = true;
            });

            calendar.render();
        })

        let acceptPayButton = document.querySelector('#confirmAndPay');
        acceptPayButton.addEventListener('click', event => {
            // console.log('payment ok');
            let confirmedEvents = []
            calendar.getEvents().forEach(async calEvent => {
                if (calEvent.rendering != 'background') {
                    let userId = await fetchUserData(getUsername());

                    let currentBooking = {
                        customer: userId.id,
                        hairdresser: hairdresser,
                        start_at: moment(calEvent.start).format('YYYY-MM-DD HH:mm:ss'),
                        end_at: moment(calEvent.end).format('YYYY-MM-DD HH:mm:ss'),
                        service: calEvent.extendedProps.serviceid
                    };
                    let confirmResponse = await confirmBooking(currentBooking);
                    // console.log();
                    if (confirmResponse == 200) {
                        reload();
                    }
                }
            });
            // reload();
        });
    }
}

async function insertServicesIntoDraggableMenu(hairdresser) {
    let draggableContainer = document.querySelector('#external-events');
    let availableServices = await getServicesByWorker(hairdresser);
    availableServices.forEach(element => {
        draggableContainer.innerHTML += /*html*/`
        <div class='fc-event' data-event='{ "title": "${element.name}"}' data-duration='${getTimeFromDurationInMinutes(element.duration)}' data-serviceid='${element.id}'>${element.name}</div>
        `;
    });
}

//created as events, inversed
async function loadSelectedWorkerShedule(workerId) {

    let schedule = await fetchSelectedWorkerSchedule(workerId);
    let scheduleEvents = [];

    schedule.forEach(element => {
        let scheduleStart = moment(element.start_at);
        let scheduleEnd = moment(element.end_at);

        scheduleEvents.push(
            {
                // title: 'test',
                start: element.start_at,
                end: element.end_at,
                rendering: 'background',
                overlap: false
            }
        );
    });
    return scheduleEvents;
}

async function loadBusinessHours(workerId) {
    let schedule = await fetchSelectedWorkerSchedule(workerId);
    // console.log(schedule);
    let businessHours = [];
    schedule.forEach(day => {
        if (moment(day.start_at) > moment().endOf('day') && moment(day.end_at) < moment().add(7, 'days').endOf('day')) {
            businessHours.push(
                {
                    daysOfWeek: [moment(day.start_at).format('E')],
                    startTime: moment(day.start_at).format('HH:mm'),
                    endTime: moment(day.end_at).format('HH:mm'),
                    extendedProps: {
                        currentScheduleId: day.id
                    }
                }
            );
        }
    });
    return businessHours;
}

async function addBackgroundEvent(hairdresser) {
    let backgroundEvents = [];
    let fetchedEvents = await fetchSelectedWorkerBookings(hairdresser);
    fetchedEvents.forEach(bookedEvent => {
        backgroundEvents.push({
            start: bookedEvent.start_at,
            end: bookedEvent.end_at,
            rendering: 'background',
            overlap: false
        });
    });
    return backgroundEvents;
}

async function cancelBooking(bookingId) {
    try {
        let bookingCancelledByClient = confirm('Oled kindel?');
        if (bookingCancelledByClient) {
            // console.log('tühistatud');
            let cancelledBookingResponse = await cancelSelectedBooking(bookingId);
            // console.log(cancelledBookingResponse.success);
            if (!cancelledBookingResponse.success) {
                renderAlert("Broneeringu tühistamine ebaõnnestus!")
            } else {
                renderSuccess("Broneering on tühistatud");
                setTimeout(function () {
                    alertClose();
                    // clearRegisterForm();
                }, 2500);
                reload();
            }
        }
    } catch (error) {
        console.log(error);
    }
}