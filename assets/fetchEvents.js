async function fetchOffers() {
    let response = await fetch(
        `${SALON_API_URL}/schedule/all`,
        {
            method: 'GET'
            // headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    )
    return await handleJsonResponse(response);
}

async function fetchSelectedWorkerSchedule(workerId) {
    let response = await fetch(
        `${SALON_API_URL}/schedule/week/${workerId}`,
        {
            method: 'GET'
            // headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    )
    return await handleJsonResponse(response);
}

async function confirmBooking(booking) {
    let response = await fetch(
        `${SALON_API_URL}/booking/add/`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Accept': 'application/json',
                // 'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(booking)
        },
    );
    console.log(booking);
    return await response.status;
}

async function getServicesByWorker(workerId) {
    let response = await fetch(
        `${SALON_API_URL}/schedule/services/${workerId}`,
        {
            method: 'GET'
            // headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    )
    return await handleJsonResponse(response);
}

async function fetchSelectedWorkerBookings(workerId) {
    let response = await fetch(
        `${SALON_API_URL}/booking/week/${workerId}`,
        {
            method: 'GET'
            // headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    return await handleJsonResponse(response);
}

async function fetchSelectedUserBookings(userId) {

    let response = await fetch(
        `${SALON_API_URL}/booking/customer/${userId}`,
        {
            method: 'GET'
            // headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    
    return await handleJsonResponse(response);

}

async function cancelSelectedBooking(bookingId){
    let response = await fetch(
        `${SALON_API_URL}/booking/cancel/${bookingId}`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Accept': 'application/json',
                // 'Authorization': `Bearer ${getToken()}`
            },
        },
        

    );
    return await handleJsonResponse(response);

}