document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.querySelector('#calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'timeGrid'],
        header: {
        },
        views: {
            timeGrid: { // name of view
                allDaySlot: false,
                minTime: "08:00:00",
                maxTime: "22:00:00",
            }
        },
        visibleRange: {
            start: moment().add(1, 'days').format(),
            start: moment().add(1, 'days').format()
        },

        businessHours: {
            daysOfWeek: [1, 2, 3, 4],
            startTime: '09:00',
            endTime: '19:00'
        },
        eventConstraint: "businessHours",
        locale: 'et',
        firstDay: 1,
        defaultDate: '2020-05-30',
        editable: true,
        selectable: true,
        weekends: true,
        events: [
            {
                id: 'available_hours',
                title: 'püroovime',
                start: '2020-05-28T13:00:00',
                end: '2020-05-28T15:30:00',
                overlap: false,
                rendering: 'background'
            },
            {
                title: 'test',
                start: '2020-05-26T15:00:00',
                overlap: false,
                constraint: 'available_hours'

            },
        ],
        select: function (arg) {
            var title = prompt('Event Title:');
            console.log("asdf");
            let asi = arg.start;
            if (title) {
                calendar.addEvent({
                    overlap: false,
                    title: title,
                    start: arg.start,
                    end: arg.end
                })
            }
            calendar.unselect()
        },
        eventClick: function (arg) {
            console.log(arg);
        }
    });
    // let newEvent = {
    //     title: 'test2',
    //     start: '2020-05-27T13:00:00',
    //     overlap: false
    // };
    // calendar.addEvent(newEvent);
    // console.log(calendar.getEvents());
    calendar.render();
});
